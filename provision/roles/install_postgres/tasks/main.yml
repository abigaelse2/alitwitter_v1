---
- name: Update and install postgres
  apt:
    name:
      - postgresql
      - postgresql-contrib
      - python3-psycopg2
    update_cache: yes
    force_apt_get: yes
    state: latest

- name: Configure listen address
  become_user: postgres
  postgresql_set:
    name: listen_addresses
    value: 0.0.0.0

- name: Allow specified user to be connected from the specified network
  postgresql_pg_hba:
    dest: /etc/postgresql/10/main/pg_hba.conf
    contype: host
    create: yes
    databases: '{{ app.db_name }}'
    users: '{{ app.db_user }}'
    method: md5
    source: 0.0.0.0/0
    state: present

- name: Allow specified user to be connected from the specified network to testing
  postgresql_pg_hba:
    dest: /etc/postgresql/10/main/pg_hba.conf
    contype: host
    create: yes
    databases: 'testing'
    users: '{{ app.db_user }}'
    method: md5
    source: 0.0.0.0/0
    state: present

- name: Allow specified user to be connected from the specified network
  postgresql_pg_hba:
    dest: /etc/postgresql/10/main/pg_hba.conf
    contype: host
    create: yes
    databases: all
    users: all
    method: md5
    source: 0.0.0.0/0
    state: present

- name: Restart server
  systemd:
    name: postgresql
    state: restarted

- name: Create new database
  become_user: postgres
  postgresql_db:
    name: '{{ app.db_name }}'
    state: present

- name: Create new testing database
  become_user: postgres
  postgresql_db:
    name: 'testing'
    state: present

- name: Create custom postgres user
  become_user: postgres
  postgresql_user:
    name: '{{ app.db_user }}'
    password: '{{ app.db_pass }}'
    db: '{{ app.db_name }}'
    role_attr_flags: NOSUPERUSER,CREATEDB
    state: present

- name: Create custom testing access db
  become_user: postgres
  postgresql_user:
    name: '{{ app.db_user }}'
    password: '{{ app.db_pass }}'
    db: 'testing'
    role_attr_flags: NOSUPERUSER,CREATEDB
    state: present

- name: Change owner database in testing
  become_user: postgres
  postgresql_owner:
    db: testing
    obj_type: database
    new_owner: '{{ app.db_user }}'
